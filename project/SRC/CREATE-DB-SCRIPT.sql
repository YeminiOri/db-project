
DROP TABLE IF EXISTS `app_users`;

CREATE TABLE `app_users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(20) NOT NULL,
  `password` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_name` (`user_name`)
);


DROP TABLE IF EXISTS `teams`;

CREATE TABLE `teams` (
  `team_id` int(11) NOT NULL AUTO_INCREMENT,
  `team_name` varchar(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `game_start` date DEFAULT NULL,
  `game_end` date DEFAULT NULL,
  PRIMARY KEY (`team_id`),
  UNIQUE KEY `team_name_UNIQUE` (`team_name`),
  KEY `user_id_idx` (`user_id`),
  CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `app_users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
);


DROP TABLE IF EXISTS `twitter_users`;

CREATE TABLE `twitter_users` (
  `twitter_user_id` bigint(20) NOT NULL,
  `twitter_user_name` varchar(50) NOT NULL,
  `category` varchar(30) DEFAULT NULL,
  `number_of_followers` bigint(20) NOT NULL DEFAULT '0',
  `user_picture` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`twitter_user_id`),
  UNIQUE KEY `twitter_user_name_UNIQUE` (`twitter_user_name`)
);


DROP TABLE IF EXISTS `team_members`;

CREATE TABLE `team_members` (
  `team_id` int(11) NOT NULL DEFAULT '0',
  `twitter_user_id` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`team_id`,`twitter_user_id`),
  KEY `twitter_id_idx` (`twitter_user_id`),
  KEY `tm_team_id_idx` (`team_id`),
  CONSTRAINT `tm_team_id` FOREIGN KEY (`team_id`) REFERENCES `teams` (`team_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tm_twitter_user_id` FOREIGN KEY (`twitter_user_id`) REFERENCES `twitter_users` (`twitter_user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
);



DROP TABLE IF EXISTS `twitter_tweets`;

CREATE TABLE `twitter_tweets` (
  `tweet_id` bigint(20) NOT NULL,
  `tweet_time` datetime NOT NULL,
  `twitter_user_id` bigint(20) NOT NULL,
  `number_of_retweets` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`tweet_id`),
  KEY `tt_twitter_user_id_idx` (`twitter_user_id`),
  KEY `tweet_time` (`tweet_time`) USING BTREE,
  CONSTRAINT `tt_twitter_user_id` FOREIGN KEY (`twitter_user_id`) REFERENCES `twitter_users` (`twitter_user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
);



