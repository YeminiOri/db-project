from django.http import HttpResponse
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from twitterFunctions import *

@csrf_exempt
def index(request):
    team_id = request.POST.get("team_id", "")
    if not team_id or int(team_id) <= 0:
        return JsonResponse({'err':'Missing Data'})
    id = int(team_id)
    count = insert(id)
    if count >= 0:
        return JsonResponse({'count':count})
    else:
        return JsonResponse({'err':'Error updating the database.'})
