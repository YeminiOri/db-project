#!/usr/bin/python
# -*- coding: utf-8 -*- 


### IMPORTS ###

import os,sys, MySQLdb
from twython import Twython
import pickle
from random import randint
import time


### CONSTANTS ####

USERNAME = "DbMysql10"
PASSWORD = "DbMysql10"
DBNAME = "DbMysql10"
HOST = "mysqlsrv.cs.tau.ac.il"
PORT = 3306


### FOR TWEETER ####

CONSUMER_KEY = 'nMzlgI3Q3286yL7pI9gWnwHXL'
CONSUMER_SECRET = 'HACmRlBdgdeLTw94d1Rj0DakL82wjPMSn0rm4PCZzWVnYZCDPU'
ACCESS_TOKEN = '730098846945165315-JStYRq6lLUhIRVrAR6XkdJwNOOIQkYt'
ACCESS_TOKEN_SECRET = 'gZ9AdxPWzsswMtEug3GHd6TnXModkdhfghJMTvI593Ecw'



### GLOBALS ###

db =None
cursor=None
is_connected = False

twitter = Twython(CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN, ACCESS_TOKEN_SECRET)


def openDBConnection():
        global db,cursor,is_connected

	if is_connected:
		return

        # Open database connection
        db = MySQLdb.connect(HOST,USERNAME,PASSWORD,DBNAME,PORT)

        # prepare a cursor object using cursor() method
        cursor = db.cursor()

	is_connected = True


def closeDBConnection():
        global db, cursor, is_connected

	if not is_connected:
		return

        cursor.close()
        # disconnect from server
        db.close()

	is_connected = False

## recieve team id and insert new tweets of the team's players into tweeter_tweets table
## returns number of tweets added
def insert(teamID):
	usersList = getTeamsTwitterUsers(teamID)
        l = getListOfTweetsFromTwitter(usersList)
	insertToTwitterTweetsMany(l)
	return len(l)


## returns the list of twitter users
def getTeamsTwitterUsers(teamID):
    
    openDBConnection()
    sql = """SELECT twitter_users.twitter_user_name
              FROM team_members, twitter_users
              WHERE team_members.team_id = %d
                AND twitter_users.twitter_user_id = team_members.twitter_user_id""" % (teamID)

    try:
        cursor.execute(sql)
        data = cursor.fetchall() # calculate the team members of the team
        closeDBConnection()
        players = []

        if (data is None):
	        closeDBConnection()
	        return players
	else:
		for player in data:
			players.append(player[0])

	closeDBConnection()
	return players

    except MySQLdb.Error, e:
        print "error while getting players"
	try:
            print "MySQL Error [%d]: %s" % (e.args[0], e.args[1])
      	except IndexError:
            print "MySQL Error: %s" % str(e)
	closeDBConnection()
	return []


## get from tweeter a list of tweets
def getListOfTweetsFromTwitter(usersList):
	
	listOfTweets =[]

	for username in usersList:
		maxID = getUserLastTweetId(username)		

		if(maxID==0):
			try:
				details = twitter.get_user_timeline(screen_name=username, incluse_rts=False)
			except Exception as e:
				print e
				continue
		else:
			try:
				details = twitter.get_user_timeline(screen_name=username, max_id=maxID-1, incluse_rts=False)	
			except Exception as e:
				print e
				continue
		for t in details:
			#print str(t['created_at'])+"  "+str(t['id'])+ "   "+str(t['retweet_count'])+"  num of tweets: "+str(tweetCounter)+"\n\n"
			tID = t['id']

			## switch from twitter date format to datetime
			tTime = time.strftime('%Y-%m-%d %H:%M:%S', time.strptime(t['created_at'],'%a %b %d %H:%M:%S +0000 %Y'))
			
			userID = getTwitterIdByUsername(username)

			numOfReTweets = t['retweet_count']	
			
			tweet = [tID, tTime, userID, numOfReTweets]
			listOfTweets.append(tweet)
		

	return listOfTweets

## get the last tweet added of that useranme
def getUserLastTweetId(username):

    openDBConnection()

    sql = "SELECT tweet_id \
		FROM twitter_tweets, twitter_users \
		WHERE twitter_users.twitter_user_name = %s AND \
			twitter_users.twitter_user_id = twitter_tweets.twitter_user_id \
			ORDER BY tweet_id ASC LIMIT 1"
     
    try:
        cursor.execute(sql, username)
	data = cursor.fetchone()
	if data == None:
		return 0
	id = data[0]
	closeDBConnection()
	return id
        
    except MySQLdb.Error, e:
        print "error while getting cost of players"
	try:
            print "MySQL Error [%d]: %s" % (e.args[0], e.args[1])
      	except IndexError:
            print "MySQL Error: %s" % str(e)
	closeDBConnection()
	return -1
      

def getTwitterIdByUsername(username):
    
    openDBConnection()

    cursor.execute("SELECT twitter_user_id FROM twitter_users WHERE twitter_user_name = %s", (username))
    data = cursor.fetchone()
    userID = data[0]

    closeDBConnection()
    return userID


##
# insertion done with exceutemany to optimize the query
#
##
def insertToTwitterTweetsMany(listOfTweets):
        global db,cursor
   	openDBConnection()
        
	sql = "INSERT INTO twitter_tweets(tweet_id, tweet_time, twitter_user_id, number_of_retweets) \
               VALUES (%s, %s, %s, %s)"
           
        try:
           cursor.executemany(sql, listOfTweets)
           db.commit()
	   closeDBConnection()
        except MySQLdb.Error, e:
           # Rollback in case there is any error
           db.rollback()
           print "error while inserting: "
           try:
                print "MySQL Error [%d]: %s" % (e.args[0], e.args[1])
           except IndexError:
                print "MySQL Error: %s" % str(e)
	   closeDBConnection()





def debug():
	#print (insert(37))
	return

if __name__ == '__main__':

	debug()
