#!/usr/bin/python
# -*- coding: utf-8 -*-


### IMPORTS ###

import os,sys, MySQLdb
import datetime
### CONSTANTS ####

USERNAME = "DbMysql10"
PASSWORD = "DbMysql10"
DBNAME = "DbMysql10"
HOST = "mysqlsrv.cs.tau.ac.il"
PORT = 3306


MAX_COST_OF_TEAM = 150000000


### GLOBALS ###

global db
db = None
global cursor
cursor = None
global is_connected
is_connected = False


def openDBConnection():
        global db,cursor,is_connected

	if is_connected:
		return

        # Open database connection
        db = MySQLdb.connect(HOST,USERNAME,PASSWORD,DBNAME,PORT)

        # prepare a cursor object using cursor() method
        cursor = db.cursor()

	is_connected = True


def closeDBConnection():
        global db, cursor, is_connected

	if not is_connected:
		return

        cursor.close()
        # disconnect from server
        db.close()

	is_connected = False


#verify validity, and check if the password is indeed correct
#return true if successfully signed-in, false otherwise
def signin(username, password):

    openDBConnection()

    cursor.execute("SELECT * FROM app_users WHERE user_name = %s AND password = %s", (username, password))
    if cursor.rowcount >= 1:
	closeDBConnection()
    	return True

    closeDBConnection()
    return False

#verify validity, and create a new user
#return true if created a new user, false otherwise
def signup(username, password):

    openDBConnection()

    sql = "INSERT INTO app_users(user_name, password) \
               VALUES ('%s', '%s')" % \
               (username, password)

    # Prepare SQL query to INSERT a record into the database.

    try:
       # Execute the SQL command
       cursor.execute(sql)
       # Commit your changes in the database
       db.commit()
       print "inserted successfully username: "+ username
       closeDBConnection()
       return True
    except MySQLdb.Error, e:
      # Rollback in case there is any error
      db.rollback()
      print "error while inserting"
      try:
           print "MySQL Error [%d]: %s" % (e.args[0], e.args[1])
      except IndexError:
           print "MySQL Error: %s" % str(e)

      closeDBConnection()
      return False

## app username to id
def getUserId(username):
    openDBConnection()

    cursor.execute("SELECT user_id FROM app_users WHERE user_name = %s", (username))
    data = cursor.fetchone()
    userID = data[0]

    closeDBConnection()
    return userID

## gets a team ID and dates, and update DB accordingly
## dates format : mm/dd/yyyy
def changeDatesOfTeam(teamID, startDate, endDate):

    openDBConnection()

    startDate = datetime.datetime.strptime(startDate, '%m/%d/%Y').strftime('%Y/%m/%d')
    endDate =  datetime.datetime.strptime(endDate, '%m/%d/%Y').strftime('%Y/%m/%d')
    updated = False

    sql = " UPDATE teams \
		SET game_start = %s ,game_end = %s \
		WHERE team_id = %s"
    args = (startDate, endDate, teamID)
    try:
       # Execute the SQL command
       cursor.execute(sql,args)
       # Commit your changes in the database
       db.commit()
       print "updated dates succssefully"
       updated = True
    except MySQLdb.Error, e:
      # Rollback in case there is any error
      db.rollback()
      print "error while updating dates"
      try:
           print "MySQL Error [%d]: %s" % (e.args[0], e.args[1])
      except IndexError:
           print "MySQL Error: %s" % str(e)

    closeDBConnection()
    return updated


## remove the team from DB
def removeTeam(teamID):

    openDBConnection()

    ##
    # to remove a team from teams table we first need to remove all is FK conecting to him
    # in the team_members table
    ##

    sql1 = "Delete FROM team_members \
		WHERE team_id = %s"

    sql2 = " DELETE FROM teams \
		WHERE team_id = %s"

    try:
       # Execute the SQL command
       cursor.execute(sql1, teamID)
       cursor.execute(sql2, teamID)
       # Commit your changes in the database
       db.commit()
       print "deleted team succssefully"
    except MySQLdb.Error, e:
      # Rollback in case there is any error
      db.rollback()
      print "error while deleting team"
      try:
           print "MySQL Error [%d]: %s" % (e.args[0], e.args[1])
      except IndexError:
           print "MySQL Error: %s" % str(e)

    closeDBConnection()


## players is a list of id's (need to cast to int)
## returns the sum of their followers
def getCostOfPlayers(players):

    if (len(players) == 0):
	    return 0

    playersIDs = []
    for stringID in players:
	    playersIDs.append(int(stringID))
    playersIDs.append(-1) ## duumy value in case the list of len==1
    openDBConnection()

    sql = """SELECT sum(twitter_users.number_of_followers)
		 FROM twitter_users
		 WHERE twitter_users.twitter_user_id IN %s"""
    args = [playersIDs]

    try:
        cursor.execute(sql, args)
	sum = cursor.fetchone()[0]
	closeDBConnection()
	return sum

    except MySQLdb.Error, e:
        print "error while getting cost of players"
	try:
            print "MySQL Error [%d]: %s" % (e.args[0], e.args[1])
      	except IndexError:
            print "MySQL Error: %s" % str(e)

    closeDBConnection()
    return 0

#team has format:
# 'players[]': array of ids, need to cast to int, and  verify there are 11
# 'username': the name of the user saving the team
# 'teamName': the name of the team.
# 'startDate': mm/dd/yyyy
# 'endDate': mm/dd/yyyy
#creates a new team and return its id
##returns -1 if more then 11 players
##returns -2 if it cost to much
## returns -3 unexpected error
## return -4 --> duplicate team name
## return -5 --> 0 players given

def new_team(team):
    players = team[0]
    if len(players) > 11:
	return -1
    if len(players) == 0:
	return -5

    username = team[1]
    userID = getUserId(username)
    teamName = team[2]
    startDate = datetime.datetime.strptime(team[3], '%m/%d/%Y').strftime('%Y/%m/%d')
    endDate =  datetime.datetime.strptime(team[4], '%m/%d/%Y').strftime('%Y/%m/%d')

    if getCostOfPlayers(players) > MAX_COST_OF_TEAM:
	return -2

    openDBConnection()

    #tableName = "teams"

    sql = "INSERT INTO teams(team_name, user_id,game_start,game_end) \
               VALUES (%s, %s, %s , %s)"
    args = (teamName, userID, startDate, endDate)

    # Prepare SQL query to INSERT a record into the database.

    try:
       # Execute the SQL command
       cursor.execute(sql, args)
       # Commit your changes in the database
       db.commit()
       print "inserted successfully teamName: "+ teamName

    except MySQLdb.Error, e:
      # Rollback in case there is any error
      db.rollback()
      print "error while inserting"
      try:
           print "MySQL Error [%d]: %s" % (e.args[0], e.args[1])
           if e.args[0] == 1062:
		closeDBConnection
		return -4
      except IndexError:
           print "MySQL Error: %s" % str(e)

      closeDBConnection()
      return -3

    ## get the team id from the team that just been inserted (it's an auto increment value)
    cursor.execute("SELECT team_id FROM teams WHERE team_name = %s", (teamName))
    data = cursor.fetchone()

    teamID =data[0]

    ### now inserting the team members all at one insertion
    
    sql = "INSERT INTO team_members(team_id, twitter_user_id) \
               VALUES (%s, %s)"

    args = []
    for player in players:
	    args.append([teamID, int(player)])


    # Prepare SQL query to INSERT a record into the database.

    try:
	    cursor.executemany(sql, args)
	    db.commit()
	    closeDBConnection()
	    print "inserted successfully team "
   	    return teamID

    except MySQLdb.Error, e:
    # Rollback in case there is any error
	   db.rollback()
	   print "error while inserting"
	   try:
                print "MySQL Error [%d]: %s" % (e.args[0], e.args[1])
      	   except IndexError:
                print "MySQL Error: %s" % str(e)

	   closeDBConnection()
	   return -3


#gets the id of the team and returns the team info (name, players, score, startDate, endDate, username)
def get_team(id):

    openDBConnection()

    tablesList = ["teams", "team_members", "twitter_tweets", "app_users"]

    
    sql1 = """SELECT teams.team_name , 5*ifnull(count(twitter_tweets.tweet_id),0)+ifnull(sum(twitter_tweets.number_of_retweets)*10,0) as points, teams.game_start, teams.game_end, app_users.user_name
             FROM teams
             JOIN app_users on teams.user_id = app_users.user_id
             LEFT OUTER JOIN team_members on teams.team_id  = team_members.team_id
		LEFT OUTER JOIN twitter_tweets on team_members.twitter_user_id = twitter_tweets.twitter_user_id
	     WHERE twitter_tweets.tweet_time BETWEEN teams.game_start and teams.game_end 
		And teams.team_id = %d""" % (id)


    sql2 = """SELECT twitter_users.twitter_user_name, twitter_users.user_picture,
                twitter_users.category, twitter_users.number_of_followers
              FROM team_members, twitter_users
              WHERE team_members.team_id = %d
                AND twitter_users.twitter_user_id = team_members.twitter_user_id""" % (id)

    try:
        cursor.execute(sql1) # calculate score per team
        data1 = cursor.fetchone()
        cursor.execute(sql2)
        data2 = cursor.fetchall() # calculate the team members of the team
        closeDBConnection()
        players = []

        if (data2 is None):
	       data = [data1[0], [], data1[1], data1[2], data1[3], data1[4]]
	else:
		for player in data2:
			players.append(player)

        	data = [data1[0], players, data1[1], data1[2], data1[3], data1[4]]

	return data # data is [data1[0], players, data1[1], data1[2], data1[3]]
	# meaning name, players, score, game_start, game_end
    except MySQLdb.Error, e:
        print "error while getting team by id"
	try:
            print "MySQL Error [%d]: %s" % (e.args[0], e.args[1])
      	except IndexError:
            print "MySQL Error: %s" % str(e)

	closeDBConnection()
	return -1

#returns list of all the teams of the username given, ordered by score, descending
#list(team(id, name, username, players,score))
def get_teams(username):

    openDBConnection()

    tablesList = ["teams", "team_members", "twitter_tweets", "app_users", "twitter_users"]
    

    sql1 = """SELECT team_score.team_id, team_score.team_name , sum(team_score.points) as score
              FROM (Select teams.team_id as team_id, teams.team_name as team_name, teams.user_id as user_id,
                      5*ifnull(count(twitter_tweets.tweet_id),0)+ifnull(sum(twitter_tweets.number_of_retweets)*10,0) as points
                     from teams left outer join team_members on teams.team_id  = team_members.team_id
                      LEFT OUTER JOIN twitter_tweets on team_members.twitter_user_id = twitter_tweets.twitter_user_id
                      And twitter_tweets.tweet_time BETWEEN teams.game_start and teams.game_end
                     GROUP BY teams.team_id, teams.user_id, team_members.twitter_user_id) as team_score,
                   app_users
             WHERE app_users.user_name = %s AND
		app_users.user_id = team_score.user_id
             	GROUP BY team_score.team_id
            	ORDER BY score desc"""


    sql2 = """SELECT twitter_users.twitter_user_name, teams.team_name
	     FROM team_members, teams, app_users, twitter_users
	     WHERE app_users.user_name = %s AND
		  teams.user_id = app_users.user_id AND
		  team_members.team_id = teams.team_id AND
		  twitter_users.twitter_user_id = team_members.twitter_user_id"""

    try:
        cursor.execute(sql1, username) # calculate score per team
        data1 = cursor.fetchall()
        cursor.execute(sql2, username) # calculate twitter_user_name in any team of the app_user
        data2 = cursor.fetchall()
        closeDBConnection()
        players = []
        teamMembersDict = {}

        if (data1 is None):
	       return []
	else: # unite team_members under
		for teamInfo in data2:
			teamName = teamInfo[1]
			playerName = teamInfo[0]
			if (teamMembersDict.has_key(teamName) == False):
				newItem = {teamName :  [playerName]}
				teamMembersDict.update(newItem)
			else: # update team_member_to the current team
				players  = teamMembersDict.get(teamName)
				players.append(playerName)
				newItem = {teamName :  players}
				teamMembersDict.update(newItem)


	data = []
	for i in range(0, len(data1)):
		teamId = data1[i][0]
		teamName = data1[i][1]
		if (teamMembersDict.has_key(teamName)):
			players = teamMembersDict.get(teamName)
		else:
			players = []
		score = data1[i][2]
		data.append([teamId, teamName, username, players, score])

	return data


    except MySQLdb.Error, e:
        print "error while getting team by id"
	try:
            print "MySQL Error [%d]: %s" % (e.args[0], e.args[1])
      	except IndexError:
            print "MySQL Error: %s" % str(e)

	closeDBConnection()
	return -1





#returns all tweeter users
#format:
# list[id,username, category, followers_count, link to picture]
## on the server side, the cost will be caculated (currently by the followers_count)

def get_tweeters():# no input parameters

	openDBConnection()

	tableName = "twitter_users"

        sql = "SELECT * FROM twitter_users"


        try:
           # Execute the SQL command
           cursor.execute(sql)
	   data = cursor.fetchall()
	   closeDBConnection()

           return data

	except MySQLdb.Error, e:

           print "error"
           try:
                   print "MySQL Error [%d]: %s" % (e.args[0], e.args[1])
           except IndexError:
                   print "MySQL Error: %s" % str(e)


#returns all tweeter users filtered by category
#format:
# list[id,username, category, followers_count, link to picture]
## on the server side, the cost will be caculated (currently by the followers_count)

def get_tweetersByCategory(category):

	openDBConnection()

        sql = "SELECT * FROM twitter_users WHERE category = %s"


        try:
           # Execute the SQL command
           cursor.execute(sql, category)
	   data = cursor.fetchall()
	   closeDBConnection()

           return data

	except MySQLdb.Error, e:

           print "error"
           try:
                   print "MySQL Error [%d]: %s" % (e.args[0], e.args[1])
           except IndexError:
                   print "MySQL Error: %s" % str(e)


        closeDBConnection()

#returns all tweeter users filtered by tweeter user name
#format:
# list[id,username, category, followers_count, link to picture]

def get_tweetersByname(name):

	openDBConnection()
	name = '%'+name+'%'
        sql = "SELECT * FROM twitter_users WHERE twitter_user_name LIKE %s"


        try:
           # Execute the SQL command
           cursor.execute(sql, name)
	   data = cursor.fetchall()
	   closeDBConnection()

           return data

	except MySQLdb.Error, e:

           print "error"
           try:
                   print "MySQL Error [%d]: %s" % (e.args[0], e.args[1])
           except IndexError:
                   print "MySQL Error: %s" % str(e)


        closeDBConnection()


## returns list(team name, user name , score) in decending order by score
def getScoreTable():

    openDBConnection()
    tablesList = ["app_users", "teams", "team_members", "twitter_tweets"]


    sql = """SELECT team_score.team_id, team_score.team as team , app_users.user_name as user_name, sum(team_score.points) as score
             FROM (Select team_name as team, teams.team_id, teams.user_id as user_id,
                      5*ifnull(count(twitter_tweets.tweet_id),0)+ifnull(sum(twitter_tweets.number_of_retweets)*10,0) as points
                     from teams left outer join team_members on teams.team_id  = team_members.team_id
                      LEFT OUTER JOIN twitter_tweets on team_members.twitter_user_id = twitter_tweets.twitter_user_id
                      And twitter_tweets.tweet_time BETWEEN teams.game_start and teams.game_end
                     GROUP BY teams.team_id, teams.user_id, team_members.twitter_user_id) as team_score,
                   app_users
             WHERE app_users.user_id = team_score.user_id
             GROUP BY team_score.team
             ORDER BY score desc"""

    try:
       cursor.execute(sql)
       data = cursor.fetchall()
       closeDBConnection()
       return data


    except MySQLdb.Error, e:
      print "error while calculating score table"
      try:
          print "MySQL Error [%d]: %s" % (e.args[0], e.args[1])
      except IndexError:
          print "MySQL Error: %s" % str(e)
      closeDBConnection()
      return -1


#uri ************************************
# show the average points for each category tweeter user
# show category iff exist twitter_user that tweeted and belong to this category
# input:
# output:
#
#
# my idea: get dates and return the avg score of every category in the dates
def GetMinCategoryPoints():

    openDBConnection()

    tablesList = ["twitter_tweets", "twitter_users"]


    sql = """SELECT category_and_point.category as category, category_and_point.points/count(*) as avg_points
             FROM twitter_users,
             (SELECT twitter_users.category as category, (5*count(*)+10*sum(twitter_tweets.number_of_retweets)) as points
              FROM twitter_users, twitter_tweets
              WHERE twitter_users.twitter_user_id = twitter_tweets.twitter_user_id
              GROUP BY  twitter_users.category) as category_and_point
             WHERE twitter_users.category = category_and_point.category
             GROUP BY twitter_users.category
             ORDER BY category_and_point.points/count(*) DESC"""

    cursor.execute(sql)
    data = cursor.fetchall()
    closeDBConnection()
    return data

#uri ************************************
# tweeter user and his popularity (ordered from most poppular to the least popular)
# shows twitter user with popularity > 0 (appears at least at one team)
#input: None
#output:
def GetTwitterUserPopularity():

    openDBConnection()

    tablesList = ["team_members", "twitter_users"]

          
    sql = """SELECT twitter_users.twitter_user_name, count(*) as team_appearances
             FROM team_members, twitter_users
             WHERE team_members.twitter_user_id = twitter_users.twitter_user_id
             GROUP BY team_members.twitter_user_id
             ORDER BY count(*) DESC"""

    cursor.execute(sql)
    data = cursor.fetchall()
    closeDBConnection()
    return data


def debug():
	#dummyQuery()
	#openDBConnection()
	#cursor.execute("DROP TABLE IF EXISTS app_users")
	#cursor.execute("DROP TABLE IF EXISTS app-users")
	#tableName = "app_users"
        #cursor.execute("DELETE FROM app_users WHERE user_name = %s", ("a"))
        #cursor.execute("DELETE FROM app_users WHERE user_name = %s", ("a"))
        #cursor.execute("DELETE FROM app_users WHERE user_name = %s", ("a"))
	#sql = "ALTER TABLE app_users MODIFY user_name VARCHAR(20) NOT NULL UNIQUE"
 	#sql = "CREATE TABLE "+tableName+""" (
        #                 user_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
        #                 user_name VARCHAR(20) NOT NULL UNIQUE,
        #                 password  VARCHAR(20) NOT NULL)"""
	#sql = "SELECT * FROM teams WHERE team_name = 'team32'"
        #cursor.execute(sql)
	#data = cursor.fetchone()
	#print data
	#closeDBConnection()
	return

def debug2():
	#print new_team([["759251","783214"],"c","my team name11", "07/13/1990","08/17/2017"])
	#print getCostOfPlayers(["759251"])
	#openDBConnection()
	#data = getScoreTable()
	#print get_tweetersByname("ba")
	#for row in data:
	#	print str(row[0]) + " xx " + str(row[1]) + " xx " + str(row[2]) +"\n"
	#cursor.execute("SELECT * FROM team_members")
        #data = cursor.fetchall()
        #print data
        #print data[1][0]
	#closeDBConnection()
        #changeDatesOfTeam(28, '01/01/2015', '10/13/2017')
	#removeTeam(28)	
	#print get_teams("c")
	#i = get_tweetersByname("ba")
	#for j in i:
	#	print j[1]
	return
if __name__ == '__main__':
	debug2()
	#debug()
