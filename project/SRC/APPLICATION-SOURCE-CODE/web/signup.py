#!/usr/bin/python

# sys.argv[1] - username
# sys.argv[2] - password
# verify the username and password are valid, and the username does not exist
# if so add the new user to the database and print 100
# otherwise print something else

import sys
from services.functions import *
from cStringIO import StringIO

regular_out = sys.stdout
sys.stdout = StringIO()
succeeded = signup(sys.argv[1], sys.argv[2])
sys.stdout.close()
sys.stdout = regular_out
if succeeded:
    print 100
else:
    print 101
