<?php ?>
<footer>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="footer-manu">
          <ul>
            <li><a href=".">Home</a></li>
            <li><a href="newteam.php">New Team</a></li>
            <li><a href="myteams.php">My Teams</a></li>
            <li><a href="scoreboard.php">Scoreboard</a></li>
            <li><a href="stats.php">Statistics</a></li>
            <li><a href="signin.php">Sign In</a></li>
            <li><a href="signup.php">Sign Up</a></li>
            <li><a href="signout.php">Sign Out</a></li>
          </ul>
        </div>
        <p>Powered by <a href="http://www.themefisher.com">Themefisher</a>.</p>
      </div>
    </div>
  </div>
</footer>
