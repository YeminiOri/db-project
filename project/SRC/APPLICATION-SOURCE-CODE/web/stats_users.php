<?php
  session_start();
  include 'globals.php';
  $stats_string = shell_exec($PY_FOLDER . 'stats_users.py');
  if(isset($stats_string)) {
    $stats_arr = preg_split("/((\r?\n)|(\r\n?))/", $stats_string);
    $stats = array();
    for ($i=0; $i+1 < count($stats_arr); $i=$i+2) {
      $t = new stdClass;
      $t->name = $stats_arr[$i];
      $t->popularity = $stats_arr[$i+1];
      array_push($stats, $t);
    }
  }
?>
<!DOCTYPE html>
<html class="no-js">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Statistics &middot; Twitter Fantasy League</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

    <!-- CSS -->
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/ionicons.min.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

    <!-- Js -->
    <script src="js/vendor/modernizr-2.6.2.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.2.min.js"><\/script>')</script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script src="js/vendor/list.min.js"></script>
    <script src="js/vendor/list.pagination.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/min/waypoints.min.js"></script>
    <script src="js/jquery.counterup.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>
  </head>
  <body>
    <?php include 'header.php';?>
    <!-- Slider Start -->
    <section id="global-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="block">
                        <h1><i class="fa fa-area-chart"></i>&nbsp;Statistics</h1>
                        <p>Tweeters</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="scoreboard">
      <div class="container">
        <div class="text-center">
          <div class="btn-group">
            <a class="btn btn-default" href="stats.php">Categories</a>
            <a class="btn btn-default" href="stats_users.php" disabled>Tweeters</a>
          </div>
        </div>
        <div id="score-list-div" class="row">
          <table class="table table-striped">
            <thead>
              <tr>
                <th>#</th>
                <th>Tweeter</th>
                <th>Number Of Teams</th>
              </tr>
            </thead>
            <tbody class="list">
              <?php for($i=0;$i<count($stats); ++$i): ?>
                <tr>
                  <td>
                    <?php echo $i+1; ?>
                  </td>
                  <td class="categoryname">
                      <?php echo $stats[$i]->name; ?>
                  </td>
                  <td class="categoryscore">
                    <?php echo $stats[$i]->popularity; ?>
                  </td>
                </tr>
              <?php endfor; ?>
            </tbody>
          </table>
          <ul class="pagination"></ul>
        </div>
      </div>
    </section>

    <?php include 'footer.php'; ?>
    <script src="js/table.list.js"></script>
    </body>
</html>
