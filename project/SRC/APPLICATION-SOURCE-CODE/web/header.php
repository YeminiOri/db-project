<?php ?>
<header>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <!-- header Nav Start -->
        <nav class="navbar navbar-default">
          <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              </button>
              <!-- <a class="navbar-brand" href=".">
                <img src="img/logo.png" alt="Logo">
              </a> -->
            </div>
              <!-- Collect the nav links, forms, and other content for toggling -->
              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                  <li><a href=".">Home</a></li>
                  <?php if(isset($_SESSION['username'])) : ?>
                    <li><a href="newteam.php">New Team</a></li>
                    <li><a href="myteams.php">My Teams</a></li>
                  <?php endif; ?>
                  <li><a href="scoreboard.php">Scoreboard</a></li>
                  <li><a href="stats.php">Statistics</a></li>
                  <?php if(isset($_SESSION['username'])) : ?>
                    <li>
                      <a href="signout.php"><span><?php echo $_SESSION['username'] ?>&nbsp;
                        <i class="fa fa-sign-out"></i>
                      </span></a>
                      <input id="h-username"type="hidden" value="<?php echo $_SESSION['username'] ?>" />
                    </li>
                  <?php else : ?>
                    <li><a href="signin.php"><span>Sign In&nbsp;</span><i class="fa fa-sign-in"></i></a></li>
                  <?php endif; ?>
                </ul>
              </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
          </nav>
        </div>
      </div>
    </div>
  </header>
