#!/usr/bin/python

import sys, os
sys.path.append(os.path.join(os.path.dirname(__file__), '..', 'services'))
from functions import *
from cStringIO import StringIO


#recieve posted form with the following args:
# 'team[]': array of ids, need to cast to int, and verify there are at most 11
# 'username': the name of the user saving the team
# 'teamName': the name of the team.
# 'startDate': mm/dd/yyyy
# 'endDate': mm/dd/yyyy
#return application/json with the created team id: {id: 24332}

import cgi, cgitb

# error handling does not work well, so we will send errors as 200
# also, since session is managed by php we cannot verify user

print 'Content-type: application/json\r\n\r\n'

form = cgi.FieldStorage()
team = form.getvalue('team[]')
username = form.getvalue('username')
teamName = form.getvalue('teamName')
startDate = form.getvalue('startDate')
endDate = form.getvalue('endDate')
if not team or not username or not teamName or not startDate or not endDate:
    print '{"err": "Missing Data"}'
else:
    team = [int(id) for id in team]
    if len(team) == 0 or len(team) > 11 or len(set(team)) != len(team):
        print '{"err": "Invalid team. Must contain 1-11 memebers"}'
    else:
        regular_out = sys.stdout
        sys.stdout = StringIO()
        id = new_team((team, username, teamName,startDate, endDate))
        sys.stdout.close()
        sys.stdout = regular_out
        if id == -1:
            print '{"err": "Invalid team. Must contain 11 memebers"}'
        elif id == -2:
            print '{"err": "Invalid team. Cost is too high"}'
        elif id == -4:
            print '{"err": "Invalid team. Team with this name already exists"}'
        elif id <= 0:
            print '{"err": "Unexpected error"}'
        else:
            print '{"id": "'+str(id)+'"}'
