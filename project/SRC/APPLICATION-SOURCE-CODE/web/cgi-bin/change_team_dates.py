#!/usr/bin/python

import sys, os
sys.path.append(os.path.join(os.path.dirname(__file__), '..', 'services'))
from functions import *
from cStringIO import StringIO

import cgi, cgitb

#input: {team_id: id, startDate: 'mm/dd/yyyy', endDate: 'mm/dd/yyyy'}
#output: {'success': true} on sucesss, {'err':'details'} on error

print 'Content-type: application/json\r\n\r\n'

form = cgi.FieldStorage()
team_id = form.getvalue('team_id')
startDate = form.getvalue('startDate')
endDate = form.getvalue('endDate')
if not team_id or not startDate or not endDate:
    print '{"err": "Missing Data"}'
else:
    id = int(team_id)
    regular_out = sys.stdout
    sys.stdout = StringIO()
    success = changeDatesOfTeam(id, startDate, endDate)
    sys.stdout.close()
    sys.stdout = regular_out
    if success:
        print '{"success": "true"}'
    else:
        print '{"err": "Error updating the database"}'
