<?php
   session_start();
   if(isset($_SESSION['username'])) {
     $_SESSION['username'] = null;
   }
   header("Location: http://www.cs.tau.ac.il/~ranfeder");
   die();
?>
