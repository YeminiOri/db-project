#!/usr/bin/python

# sys.argv[1] - username
# sys.argv[2] - password
# verify the username and password are valid and exist
# if so print 100, otherwise print something else

import sys
from services.functions import *

if signin(sys.argv[1], sys.argv[2]):
    print 100
else:
    print 101
