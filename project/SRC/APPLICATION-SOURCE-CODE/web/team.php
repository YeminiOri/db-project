<?php
   session_start();
   include 'globals.php';
   $team_id = (int)$_REQUEST["id"];
   if($team_id <= 0) {
     header('Location: ' . $SITE_URL);
     die();
   }
   $edit_mode = false;
   $team_string = shell_exec($PY_FOLDER . 'team.py ' . $team_id);
   if(isset($team_string)) {
     $team_arr = preg_split("/((\r?\n)|(\r\n?))/", $team_string);
     if(count($team_arr) < 5){
       header('Location: ' . $SITE_URL);
       die();
     }
     $team = new stdClass;
     $team->id = $team_id;
     $team->name = $team_arr[0];
     $team->username = $team_arr[1];
     $team->score = $team_arr[2];
     $team->startDate = $team_arr[3];
     $team->endDate = $team_arr[4];
     $team->players = array();
     for($i=0;8+$i*4<count($team_arr);++$i){
       $p = new stdClass;
       $p->username = $team_arr[5+$i*4];
       $p->image = $team_arr[6+$i*4];
       $p->category = $team_arr[7+$i*4];
       $p->points = $team_arr[8+$i*4];
       array_push($team->players, $p);
     }
     if (isset($_SESSION["username"]) && strcmp($_SESSION["username"], $team->username) === 0) {
       $edit_mode = true;
     }
   }
   else {
     header('Location: ' . $SITE_URL);
     die();
   }
?>
<!DOCTYPE html>
<html class="no-js">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Team #<?php echo $team_id; ?> &middot; Twitter Fantasy League</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSS -->
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/ionicons.min.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/css/bootstrap-datepicker.min.css">
    <!-- Js -->
    <script src="js/vendor/modernizr-2.6.2.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.2.min.js"><\/script>')</script>
    <script src="js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/js/bootstrap-datepicker.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/min/waypoints.min.js"></script>
    <script src="js/jquery.counterup.js"></script>
    <script src="js/main.js"></script>

  </head>
  <body>
    <?php include 'header.php';?>
    <!-- Slider Start -->
    <section id="global-header">
      <div class="container">
        <div class="row">
          <div class="col-md-10 col-md-offset-2">
            <div class="block">
              <h1 class="animated fadeInUp">
                <?php echo $team->name; ?>
              </h1>
              <p class="animated fadeInUp"><?php echo $team->username; ?></p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Wrapper Start -->
    <section id="team">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12">
            <div class="row">
              <div class="col-md-6 col-sm-6 col-xs-12">
                <span>Start date:</span>
                <input id="start-date" type="text" class="form-control datepicker" value="<?php echo $team->startDate; ?>" <?php echo ($edit_mode ? '' : 'disabled'); ?> />
              </div>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <span>End date:</span>
                <input id="end-date" type="text" class="form-control datepicker" value="<?php echo $team->endDate; ?>" <?php echo ($edit_mode ? '' : 'disabled'); ?> />
              </div>
            </div>
            <div>
              <div class="row row-gap">
                <div class="col-md-6 col-sm-12 col-xs-12">
                  <?php if($edit_mode): ?>
                    <input id="h-team-id" type="hidden" value="<?php echo $team_id; ?>" />
                    <button id="change-dates" class="btn btn-default">Change Dates</button>
                    <button id="crawl" class="btn btn-default">Update Database</button>
                  <?php endif; ?>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                  <span>Score: <span id="team-score"><?php echo $team->score; ?></span></span>
                </div>
              </div>
            </div>
              <?php foreach ($team->players as $tweeter): ?>
                <div class="team-member col-md-6 col-sm-12 col-xs-12">
                  <div class="tweeter-card-values">
                    <span class="tweeterpoints"><?php echo $tweeter->points; ?></span>
                    <div>
                      <img class="tweeterimg" src="<?php echo $tweeter->image; ?>" />
                      <span class="tweeterusername"><?php echo $tweeter->username; ?></span>
                    </div>
                    <span class="tweetercategory"><?php echo $tweeter->category; ?></span>
                  </div>
                </div>
              <?php endforeach; ?>
            </div>
          </div>
          <div class="row row-gap">
            <div class="co-md-12 col-sm-12 col-xs-12">
              <?php if($edit_mode): ?>
                <a href="deleteteam.php?id=<?php echo $team_id; ?>"><button class="btn btn-danger">Delete Team</button></a>
              <?php endif; ?>
            </div>
          </div>
        </div>
      </div>

    </section>
    <!-- footer Start -->
    <?php include 'footer.php'; ?>
    </body>
</html>
