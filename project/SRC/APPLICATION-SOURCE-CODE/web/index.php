<?php
   session_start();
?>
<!DOCTYPE html>
<html class="no-js">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Twitter Fantasy League</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

    <!-- CSS -->
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/ionicons.min.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/responsive.css">

    <!-- Js -->
    <script src="js/vendor/modernizr-2.6.2.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.2.min.js"><\/script>')</script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/min/waypoints.min.js"></script>
    <script src="js/jquery.counterup.js"></script>

    <!-- Google Map -->
    <!-- <script src="https://maps.googleapis.com/maps/api/js"></script>
    <script src="js/google-map-init.js"></script> -->


    <script src="js/main.js"></script>


  </head>
  <body>
    <?php include 'header.php';?>
    <!-- Slider Start -->
    <section id="slider">
      <div class="container">
        <div class="row">
          <div class="col-md-10 col-md-offset-2">
            <div class="block">
              <h1 class="animated fadeInUp">
                <i class="icon ion-social-twitter"></i>TWITTER FANTASY LEAGUE<i class="icon ion-social-twitter"></i>
              </h1>
              <p class="animated fadeInUp">Choose your favorite tweeters. Compete against the rest of the world</p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Service Start -->
    <section id="service">
      <div class="container">
        <div class="row">
          <div class="section-title">
            <h2>How it work</h2>
            <p></p>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6 col-md-3">
            <a href="signin.php"><div class="service-item">
              <i class="icon ion-log-in"></i>
              <h4>Sign In</h4>
              <p>You must sign in to your user, or create a new one, before you will be able to start playing.</p>
            </div></a>
          </div>
          <div class="col-sm-6 col-md-3">
            <a href="newteam.php"><div class="service-item">
              <i class="icon ion-social-twitter-outline"></i>
              <h4>Choose Tweeters</h4>
              <p>Create a new team by choosing up to 11 tweeters. Each team takes effect between specific dates.</p>
            </div></a>
          </div>
          <div class="col-sm-6 col-md-3">
            <a href="scoreboard.php"><div class="service-item">
              <i class="icon ion-stats-bars"></i>
              <h4>Compare Your Results</h4>
              <p>See how well your team fare against other teams.</p>
            </div></a>
          </div>
          <div class="col-sm-6 col-md-3">
            <a href="stats.php"><div class="service-item">
              <i class="icon ion-arrow-graph-up-right"></i>
              <h4>Improve</h4>
              <p>Study game statistics to build the best teams.</p>
            </div></a>
          </div>
        </div>
      </div>
    </section>
    <!-- footer Start -->
    <?php include 'footer.php' ?>
    </body>
</html>
