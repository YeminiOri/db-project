#!/usr/bin/python

import sys
from services.functions import *

# argv[1] is team id
# format:
# 49 lines: name,username, score, startDate, endDate, players*11*4(user,image, category, points)
t = get_team(int(sys.argv[1]))
if t != -1:
    print t[0] #name
    print t[5] # username
    print t[2] # score
    print t[3].strftime('%m/%d/%Y') #startDate
    print t[4].strftime('%m/%d/%Y') #endDate
    players = t[1]
    for player in players:
        print player[0]
        print player[1]
        print player[2]
        print player[3]
