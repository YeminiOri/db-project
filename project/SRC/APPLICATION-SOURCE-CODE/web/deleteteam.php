<?php
   session_start();
   include 'globals.php';
   $team_id = (int)$_REQUEST["id"];
   $username = $_SESSION['username'];
   if (isset($username) && $team_id > 0) {
     shell_exec($PY_FOLDER . 'deleteteam.py ' . $username . ' ' . $team_id);
   }
   //no notice, we dont have 401/404/500 pages
   header('Location: ' . $SITE_URL);
   die();
?>
