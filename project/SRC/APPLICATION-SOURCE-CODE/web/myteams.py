#!/usr/bin/python

import sys
from services.functions import *

# returns the user teams on the system
# format:
# each 4 lines are a team: id, username, teamname, score
table = get_teams(sys.argv[1])
for team in table:
    print team[0]
    print team[2]
    print team[1]
    print team[4]
