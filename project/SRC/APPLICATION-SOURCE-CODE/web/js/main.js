$(document).ready(function() {
    var CGI_URL = 'http://www.cs.tau.ac.il/cgi-bin/cgiwrap/ranfeder/';
    var SITE_URL = 'http://www.cs.tau.ac.il/~ranfeder/';
    // Counter
    $('.counter').counterUp({
        delay: 10,
        time: 1000
    });


    // Team
    $('.tweeters-list .tweeter-card .tweeter-card-values').click(function() {
        if ($(this).hasClass('selected')) {
            return;
        }
        var id = $(this).find('.tweeterid').val();
        var category = $(this).find('.tweetercategory').html();
        var cost = parseInt($(this).find('.tweeterpoints').html()) / 1000000;
        var positions = $('.team-member.member-empty');
        if (!positions.length) {
            toastr.warning('No more spots available');
        } else {
            var pos = positions.first();
            pos.html($(this).clone());
            pos.removeClass('member-empty');
            $(this).addClass('selected');
            var remainingM = parseFloat($('#remaining-points').html()) - cost;
            $('#remaining-points').html(remainingM.toFixed(3));
        }
    });


    $('#save-team').click(function() {
        var selectedMembers = $('.team-member').not('.member-empty');
        var teamName = $('#team-name').val();
        var startDate = $('#start-date').val();
        var endDate = $('#end-date').val();
        var username = $('#h-username').val();
        if (!teamName || !teamName.trim() || teamName.trim() > 20) {
            toastr.error('Please choose a name to your team');
        } else if (selectedMembers.length == 0) {
            toastr.error('Please choose tweeters');
        } else if (!startDate || !endDate) {
            toastr.error('Please select start and end date for the team');
        } else if (!username) {
            toastr.error('Unexpected error');
        } else {
            var ids = selectedMembers.map(function() {
                return $(this).find('.tweeterid').val();
            }).get();
            $.post(CGI_URL + 'newteam.py', {
                    team: ids,
                    username: username,
										teamName: teamName.trim(),
										startDate: startDate,
										endDate: endDate,
                })
                .done(function(data) {
                    if (data.id){
                      window.location = SITE_URL + 'team.php?id=' + data.id;
                    } else {
                      toastr.error('Error saving team. ' + (data.err || ''));
                    }
                })
                .error(function() {
                    toastr.error('Error saving team');
                });
        }
    });

    $("#change-dates").click(function(){
      var team_id = $('#h-team-id').val();
      var startDate = $('#start-date').val();
      var endDate = $('#end-date').val();
      $.post(CGI_URL + 'change_team_dates.py', {
              team_id: team_id,
              startDate: startDate,
              endDate: endDate
          })
          .done(function(data) {
              if (data.success){
                window.location.reload();
              } else {
                toastr.error('Error changing dates. ' + (data.err || ''));
              }
          })
          .error(function() {
              toastr.error('Error changing dates.');
          });
    });

    $('#crawl').click(function() {
      var team_id = $('#h-team-id').val();
      $.post('http://delta-tomcat-vm.cs.tau.ac.il:60394/crawl/', {
        "team_id": team_id
      }).done(function (data) {
        if (data.count && data.count > 0) {
          toastr.success('Update finished, ' + data.count +
            ' new tweets were found. Refreshing page...');
          setTimeout(function () {
            window.location.reload();
          }, 2500);
        } else if (data.count === 0 || data.count === "0") {
          toastr.success('Update finished, no more tweets to load.');
        } else {
          toastr.error('Error updating the database. ' + (data.err || ''));
        }
      }).error(function(){
        toastr.error('Failed to update the database.');
      });
      toastr.info('Crawling has started. Please wait');
    });

		if($('.datepicker').datepicker){
			$('.datepicker').datepicker({});
		}

    if($('.nano').nanoScroller){
      $('.nano').nanoScroller();
    }
});
