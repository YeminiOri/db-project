(function() {
    var options = {
        valueNames: ['teamname'],
        page: 10,
        plugins: [ListPagination({})]
    };
    var scoreList = new List('score-list-div', options);
})();
