<?php
   session_start();
   include 'globals.php';
   $sign_status = null;
   if (isset($_SESSION['username'])) {
     header('Location: ' . $SITE_URL);
     die();
   }
   if (isset($_POST['username']) || isset($_POST['password']) || isset($_POST['password2'])){
     $sign_status = 'Please fill all fields';
   }
   if (isset($_POST['username']) && isset($_POST['password']) && isset($_POST['password2'])){
     if(strcmp($_POST['password'],$_POST['password2']) !== 0){
       $sign_status = 'Passwords do not match';
     } else if (!preg_match('/^[\w]+$/', $_POST['username'])) {
       $sign_status = 'Username must be alphanumeric';
     } else if (!preg_match('/^[\w]+$/', $_POST['password'])) {
       $sign_status = 'Password must be alphanumeric';
     } else if (strlen($_POST['password']) < 4 || strlen($_POST['password']) > 20){
       $sign_status = 'Password must be between 4 to 20 characters';
     } else if (strlen($_POST['username']) > 18) {
       $sign_status = 'Username must may have up to 18 characters';
     } else {
       $cmd = $PY_FOLDER . 'signup.py ' . $_POST['username'] . ' ' . $_POST['password'];
       $retval = shell_exec($cmd);
       if ((int)$retval == 100) {
         $_SESSION['username'] = $_POST['username'];
         header('Location: ' . $SITE_URL);
         die();
       } else {
         $sign_status = 'User already exists';
       }
     }
   }
?>
<!DOCTYPE html>
<html class="no-js">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Twitter Fantasy League</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

    <!-- CSS -->
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/ionicons.min.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/responsive.css">

    <!-- Js -->
    <script src="js/vendor/modernizr-2.6.2.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script>
        window.jQuery || document.write('<script src="js/vendor/jquery-1.10.2.min.js"><\/script>')
    </script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/min/waypoints.min.js"></script>
    <script src="js/jquery.counterup.js"></script>

    <!-- Google Map -->
    <!-- <script src="https://maps.googleapis.com/maps/api/js"></script>
    <script src="js/google-map-init.js"></script> -->


    <script src="js/main.js"></script>
</head>
<body>
  <?php include 'header.php';?>
  <!-- Slider Start -->
    <section id="global-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="block">
                        <h1>Sign Up</h1>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- contact form start -->
    <section id="contact-form">
        <div class="container">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6 col-sm-12">
                    <div class="block">
                        <form method="post">
                          <?php if(isset($_POST['username']) || isset($_POST['password'])
                          || isset($_POST['password2'])) : ?>
                            <div class="alert alert-danger" role="alert">
                              <span><?php echo $sign_status; ?></span>
                            </div>
                        <?php endif; ?>
                            <div class="form-group">
                                <input type="text" class="form-control" name="username" placeholder="Username" required>
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="password" placeholder="Password" required>
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="password2" placeholder="Repeat Password" required>
                            </div>
                            <button class="btn btn-default" type="submit">Submit</button>
                        </form>
                    </div>
                </div>
                <div class="col-md-3"></div>
            </div>
        </div>
      </div>
    </section>
    <!-- footer Start -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="footer-manu">
                        <ul>
                            <li><a href="#">About Us</a></li>
                            <li><a href="#">Contact us</a></li>
                            <li><a href="#">How it works</a></li>
                            <li><a href="#">Support</a></li>
                            <li><a href="#">Terms</a></li>
                        </ul>
                    </div>
                    <p>Copyright &copy; 2006 - 2015 Built by <span>Themefisher</span>. All rights reserved.</p>
                </div>
            </div>
        </div>
    </footer>



</body>

</html>
