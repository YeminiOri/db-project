<?php
  session_start();
  include 'globals.php';
  if(!isset($_SESSION['username'])) {
    header('Location: ' . $SITE_URL);
    die();
  }
  $tweeters_string = shell_exec($PY_FOLDER . 'get_tweeters.py');
  if(isset($tweeters_string)) {
    $tweeters_arr = preg_split("/((\r?\n)|(\r\n?))/", $tweeters_string);
    $tweeters = array();
    for ($i=0; $i+4 < count($tweeters_arr); $i=$i+5) {
      $t = new stdClass;
      $t->id = $tweeters_arr[$i];
      $t->username = $tweeters_arr[$i+1];
      $t->image = $tweeters_arr[$i+2];
      $t->points = $tweeters_arr[$i+3];
      $t->category = $tweeters_arr[$i+4];
      array_push($tweeters, $t);
    }
  }
?>
<!DOCTYPE html>
<html class="no-js">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>New Team | Twitter Fantasy League</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

    <!-- CSS -->
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/ionicons.min.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="css/nanoscroller.css">


    <!-- Js -->
    <script src="js/vendor/modernizr-2.6.2.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.2.min.js"><\/script>')</script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/js/bootstrap-datepicker.min.js"></script>
    <script src="js/vendor/list.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/min/waypoints.min.js"></script>
    <script src="js/jquery.counterup.js"></script>
    <script src="js/vendor/jquery.nanoscroller.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>
  </head>
  <body>
    <?php include 'header.php';?>
    <!-- Slider Start -->
    <section id="global-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="block">
                        <h1><i class="fa fa-users"></i>&nbsp;New Team</h1>
                        <p>.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="new-team">
      <div class="container">
        <div class="row">
          <div id="tweeter-list-div" class="col-md-4 col-sm-12">
            <div class="col-md-12">
              <div class="form-group">
                <input type="text" id="search-box" name="search-box" class="search form-control fa-placeholder" placeholder="&#xF002; Search">
              </div>
            </div>
            <div class="col-md-12 col-sm-12">
              <?php if(count($tweeters) == 0): ?>
                <span>Error loading data.</span>
              <?php endif; ?>
              <div class="nano">
              <ul class="list tweeters-list nano-content">
                <?php foreach($tweeters as $tweeter): ?>
                  <li class="tweeter-card">
                    <div class="tweeter-card-values">
                      <input type="hidden" class="tweeterid" value="<?php echo $tweeter->id; ?>" />
                      <span class="tweeterpoints"><?php echo $tweeter->points; ?></span>
                      <div>
                        <img class="tweeterimg" src="<?php echo $tweeter->image; ?>" />
                        <span class="tweeterusername"><?php echo $tweeter->username; ?></span>
                      </div>
                      <span class="tweetercategory"><?php echo $tweeter->category; ?></span>
                    </div>
                  </li>
                <?php endforeach; ?>
              </ul></div>
            </div>
          </div>
          <div class="col-md-8 col-sm-12">
            <div class="col-md-6 col-sm-6 col-xs-12">
              <span>Start date:</span><input id="start-date" type="text" class="datepicker form-control" />
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <span>End date:</span><input id="end-date" type="text" class="datepicker form-control" />
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <h3>Your Team</h3>
            </div>
              <div class="col-md-7 col-sm-12 col-xs-12">
                <input id="team-name" type="text" class="form-control" placeholder="Team Name" />
              </div>
              <div class="col-md-5 col-sm-12 col-xs-12">
                <button class="btn btn-default" id="save-team">Submit</button>
              </div>
              <div class="col-md-6 col-sm-12 col-xs-12">
              </div>
              <div class="col-md-6 col-sm-12 col-xs-12">
                <span>Remaining Points: <span id="remaining-points">150</span>M</span>
              </div>

              <div id="team-member1" class="team-member member-empty col-md-6 col-sm-12 col-xs-12"></div>
              <div id="team-member2" class="team-member member-empty col-md-6 col-sm-12 col-xs-12"></div>
              <div id="team-member3" class="team-member member-empty col-md-6 col-sm-12 col-xs-12"></div>
              <div id="team-member4" class="team-member member-empty col-md-6 col-sm-12 col-xs-12"></div>
              <div id="team-member5" class="team-member member-empty col-md-6 col-sm-12 col-xs-12"></div>
              <div id="team-member6" class="team-member member-empty col-md-6 col-sm-12 col-xs-12"></div>
              <div id="team-member7" class="team-member member-empty col-md-6 col-sm-12 col-xs-12"></div>
              <div id="team-member8" class="team-member member-empty col-md-6 col-sm-12 col-xs-12"></div>
              <div id="team-member9" class="team-member member-empty col-md-6 col-sm-12 col-xs-12"></div>
              <div id="team-member10" class="team-member member-empty col-md-6 col-sm-12 col-xs-12"></div>
              <div id="team-member11" class="team-member member-empty col-md-6 col-sm-12 col-xs-12"></div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <?php include 'footer.php'; ?>
    <script src="js/tweeter.list.js"></script>
    </body>
</html>
