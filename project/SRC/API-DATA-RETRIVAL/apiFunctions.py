#!/usr/bin/python
# -*- coding: utf-8 -*- 


### IMPORTS ###

import os,sys, MySQLdb
from twython import Twython
import pickle
from random import randint
import time


### CONSTANTS ####
### FOR TWEETER ####

CONSUMER_KEY = 'nMzlgI3Q3286yL7pI9gWnwHXL'
CONSUMER_SECRET = 'HACmRlBdgdeLTw94d1Rj0DakL82wjPMSn0rm4PCZzWVnYZCDPU'
ACCESS_TOKEN = '730098846945165315-JStYRq6lLUhIRVrAR6XkdJwNOOIQkYt'
ACCESS_TOKEN_SECRET = 'gZ9AdxPWzsswMtEug3GHd6TnXModkdhfghJMTvI593Ecw'

## GLOBALS
twitter = Twython(CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN, ACCESS_TOKEN_SECRET)




#####
##
## a full working example (including calls to the DB) can be found under SRC/APPLICATION-SOURCE-CODE/web/services/twitterFunctions.py --> insert(teamID)
##
######

## input is a list of twitter users, for every user we "find" his last tweet in the DB and generate older tweets using twitter REST API
def getListOfTweetsFromTwitter(usersList):
	
	listOfTweets =[]

	for username in usersList:
		maxID = getUserLastTweetId(username) ## from the DB	

		if(maxID==0):
			try:
				details = twitter.get_user_timeline(screen_name=username, incluse_rts=False)
			except Exception as e:
				print e
				continue
		else:
			try:
				details = twitter.get_user_timeline(screen_name=username, max_id=maxID-1, incluse_rts=False)	
			except Exception as e:
				print e
				continue

		for t in details:
			
			tID = t['id']

			## switch from twitter date format to datetime
			tTime = time.strftime('%Y-%m-%d %H:%M:%S', time.strptime(t['created_at'],'%a %b %d %H:%M:%S +0000 %Y'))
			
			userID = getTwitterIdByUsername(username)

			numOfReTweets = t['retweet_count']	
			
			tweet = [tID, tTime, userID, numOfReTweets]
			listOfTweets.append(tweet)
		

	return listOfTweets


## input is a list of usernames and for every user we generate his information such as number of followers , his profile picture..
def getListOfUsersFromTwitter(userNamesList):
	global twitter

	listOfUsers = []

	for userName in userNamesList:
		details = twitter.show_user(screen_name=userName)
		user_id = details['id']
		numOfFollowers = details['followers_count']
		pic = details['profile_image_url']
		user = [user_id, userName, numOfFollowers, pic]
		listOfUsers.append(user)

	return listOfUsers



